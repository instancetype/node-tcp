/**
 * Created by instancetype on 6/24/14.
 */
var net = require('net')
  , host = process.argv[2]
  , port = Number(process.argv[3])

var socket = net.connect(port, host)

socket.on('connect', function() {
  process.stdin.pipe(socket)
  socket.pipe(process.stdout)
})

socket.on('end', function() {
  process.stdin.pause()
})